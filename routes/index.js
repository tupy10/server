const express = require("express");
const Router = express.Router();

Router.get('/', (req, res) => {return res.json({message: 'Welcome'})})

//Auth
const AuthController = require("../app/controllers/AuthController");
Router.post('/register', AuthController.register);
Router.post('/login', AuthController.login);
Router.post('/forgot-password', AuthController.forgotPassword);
Router.post('/reset-password', AuthController.resetPassword);

const routesAuth = require('./auth');

Router.use(routesAuth)

module.exports = Router;
