const express = require("express");

const Router = express.Router();
const authCheck = require("../app/middlewares/auth")

//Users
const UserController = require("../app/controllers/UserController");
Router.get('/users', authCheck, UserController.list);
Router.get('/users/:id', authCheck, UserController.show);
Router.post('/users', authCheck, UserController.create);
Router.put('/users/:id', authCheck, UserController.update);
Router.delete('/users/:id', authCheck, UserController.delete);

//Banks
const BankController = require("../app/controllers/BankController");
Router.get('/banks', authCheck, BankController.list);
Router.get('/banks/:id', authCheck, BankController.show);
Router.post('/banks', authCheck, BankController.create);
Router.put('/banks/:id', authCheck, BankController.update);
Router.delete('/banks/:id', authCheck, BankController.delete);

//Entities
const EntityController = require("../app/controllers/EntityController");
Router.get('/entities', authCheck, EntityController.list);
Router.post('/entities', authCheck, EntityController.create);
Router.get('/entities/:id', authCheck, EntityController.show);
Router.put('/entities/:id', authCheck, EntityController.update);
Router.delete('/entities/:id', authCheck, EntityController.delete);

//Transactions
const TransactionController = require("../app/controllers/TransactionController");
Router.get('/transactions', authCheck, TransactionController.list);
Router.post('/transactions', authCheck, TransactionController.create);
Router.get('/transactions/:id', authCheck, TransactionController.show);
Router.put('/transactions/:id', authCheck, TransactionController.update);
Router.delete('/transactions/:id', authCheck, TransactionController.delete);

//Courses
const CourseController = require("../app/controllers/CourseController");
Router.get('/courses', authCheck, CourseController.list);
Router.post('/courses', authCheck, CourseController.create);
Router.get('/courses/:id', authCheck, CourseController.show);
Router.put('/courses/:id', authCheck, CourseController.update);
Router.delete('/courses/:id', authCheck, CourseController.delete);

module.exports = Router;
