require('dotenv').config();

module.exports = {
    app: {
        HTTP_HOST: process.env.HTTP_HOST || "localhost",
        HTTP_PORT: process.env.HTTP_PORT || 3000,
        SALT_ROUNDS: process.env.SALT_ROUNDS || 10,
        jwtSecret: process.env.JWT_SECRET,
        jwtExpiresIn: process.env.JWT_EXPIRES || 86400,
    },
    mail: {
        driver: process.env.MAIL_DRIVER || "smtp",
        host: process.env.MAIL_HOST || "smtp.mailtrap.io",
        port: process.env.MAIL_PORT || 465,
        from: {
            address: process.env.MAIL_FROM_ADDRESS || 'hello@example.com',
            name: process.env.MAIL_FROM_NAME || 'Example',
        },
        encryption: process.env.MAIL_ENCRYPTION || "tls",
        user: process.env.MAIL_USERNAME,
        pass: process.env.MAIL_PASSWORD,
    },
    db: {
        HOST: process.env.DB_HOST || "localhost",
        USER: process.env.DB_USER || "postgres",
        PASSWORD: process.env.DB_PASSWORD || "",
        DATABASE: process.env.DB_DATABASE || "flourish",
        PORT: process.env.DB_PORT || 5432,
        dialect: "postgres",
        SSL: false,
        pool: {
            max: 5,
            min: 0,
            acquire: 30000,
            idle: 10000,
        },
    }
};
