require('dotenv').config()

const HOST = process.env.DB_HOST || 'localhost';
const USER = process.env.DB_USER || 'postgres';
const PASSWORD = process.env.DB_PASSWORD || '';
const DATABASE = process.env.DB_DATABASE || 'flourish';
const PORT = process.env.DB_PORT || 5432;
const dialect = 'postgres';

module.exports = {
    development: {
        url: `${dialect}://${USER}:${PASSWORD}@${HOST}:${PORT}/${DATABASE}`,// process.env.DEV_DATABASE_URL,
        dialect: 'postgres',
    },
    test: {
        url: process.env.TEST_DATABASE_URL,
        dialect: 'postgres',
    },
    production: {
        url: process.env.DATABASE_URL,
        dialect: 'postgres',
    },
}
