const path = require('path');
const nodemailer = require('nodemailer')
const hbs = require("nodemailer-express-handlebars");

const { user, pass, port, host } = require('./index').mail

const transport = nodemailer.createTransport({
    host,
    port,
    auth: { user, pass}
})

// console.table({user, pass})

const defaultPath = path.resolve('resources/mail/');
// console.log('path', defaultPath)

transport.use('compile', hbs({
    viewEngine: {
        extName: '.html',
        partialsDir: defaultPath,
        defaultLayout: '',
    },
    viewPath: defaultPath,
    extName: '.html'
}))

module.exports = transport
