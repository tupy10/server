'use strict';
const banks = require('./banks.json')

Object.keys(banks).forEach((bank, key) => {
  banks[key]["personType"] = "J"
})

// console.debug('Banks', banks)

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.bulkInsert('Banks', banks, {});
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('Banks', null, {});
  }
};
