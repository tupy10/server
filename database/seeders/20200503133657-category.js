'use strict';

const categories = [{
  name: 'Casa',
  type: 'Despesa'
}, {
  name: 'Telefone / Internet / TV',
  type: 'Despesa'
}, {
  name: 'Gás',
  type: 'Despesa'
}, {
  name: 'Mercado',
  type: 'Despesa'
}, {
  name: 'Salário',
  type: 'Receita'
}];

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Categories', categories, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Categories', null, {});
  }
};
