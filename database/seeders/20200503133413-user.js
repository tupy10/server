'use strict';
const bcrypt = require('bcrypt');

const users = [{
    "name": "Aguinaldo Tupy",
    "email": "aguinaldo.tupy@icloud.com",
    "password": "12345678"
}, {
    "name": "Fabrício Carneiro",
    "email": "scfabricio15@gmail.com",
    "password": "12345678"
}, {
    "name": "Caique Roschel",
    "email": "croschel000@gmail.com",
    "password": "12345678"
}, {
    "name": "Henry Mello",
    "email": "henry.hoyer.mello@gmail.com",
    "password": "12345678"
}, {
    "name": "Amauri Elias",
    "email": "amaurielias@ymail.com",
    "password": "12345678"
}];

Object.keys(users).forEach((user, key) => {
    users[key]["password"] = bcrypt.hashSync(users[key]["password"], bcrypt.genSaltSync(10), null)
})

module.exports = {
    up: (queryInterface) => {
        return queryInterface.bulkInsert('Users', users, {});
    },

    down: (queryInterface) => {
        return queryInterface.bulkDelete('Users', null, {});
    }
};
