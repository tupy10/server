const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { jwtSecret, jwtExpiresIn, SALT_ROUNDS } = require('../../config').app;

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: DataTypes.STRING,
    score: {
      type: DataTypes.STRING
    },
    email: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
      validate: {
        isEmail: true
      }
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      set(value) {
        this.setDataValue('password', bcrypt.hashSync(value, bcrypt.genSaltSync(SALT_ROUNDS), null));
      }
    },
    rememberToken: {
      type: DataTypes.STRING,
    },
    rememberTokenExpires: {
      type: DataTypes.DATE,
    }
  }, {
    defaultScope: {
      attributes: { exclude: ['password', 'rememberToken', 'rememberTokenExpires'] }
    }
  });

  User.authenticate = async function(email, password) {

    const user = await User.findOne({
      where: { email },
      attributes: {
        include: ['password']
      }
    });

    if (await bcrypt.compareSync(password, user.password)) {
      return user.authorize();
    }

    throw new Error('invalid password');
  }

  User.prototype.authorize = async function () {
    const user = this

    const token = jwt.sign({id: user.id}, jwtSecret, {
      expiresIn: jwtExpiresIn
    })

    if (!token) {
      throw new Error('Token not generate')
    }

    return {
      user: {
        id: user.id,
        email: user.email,
        name: user.name,
        score: user.score,
      },
      token: `Bearer ${token}`,
      // expiresIn: jwtExpiresIn
    }
  };

  User.associate = function(models) {
    User.hasMany(models.Transaction, {
      foreignKey: 'userId',
      as: 'transactions',
      onDelete: 'CASCADE',
    });

    User.hasMany(models.Category, {
      foreignKey: 'userId',
      as: 'categories',
      onDelete: 'CASCADE',
    });
  };
  return User;
};
