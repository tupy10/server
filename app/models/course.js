'use strict';
module.exports = (sequelize, DataTypes) => {
  const Course = sequelize.define('Course', {
    name: DataTypes.STRING,
    duration: DataTypes.STRING,
    score: {
      type: DataTypes.STRING
    },
    status: {
      type: DataTypes.STRING
    },
  }, {});
  Course.associate = function(models) {
    // associations can be defined here
  };
  return Course;
};
