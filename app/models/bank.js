'use strict';
module.exports = (sequelize, DataTypes) => {
  const Bank = sequelize.define('Bank', {
    code: DataTypes.STRING,
    name: DataTypes.STRING,
    trading: DataTypes.STRING,
    personType: DataTypes.STRING,
    numberSocial: DataTypes.STRING
  }, {});

  Bank.associate = function(models) {
    Bank.hasMany(models.Transaction, {
      foreignKey: 'bankId',
      as: 'transactions',
      onDelete: 'CASCADE',
    });
  };

  return Bank;
};
