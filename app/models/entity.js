'use strict';
module.exports = (sequelize, DataTypes) => {
  const Entity = sequelize.define('Entity', {
    name: DataTypes.STRING,
    userId: DataTypes.INTEGER
  }, {});
  Entity.associate = function(models) {
    // associations can be defined here
  };
  return Entity;
};