'use strict';
module.exports = (sequelize, DataTypes) => {
  const Category = sequelize.define('Category', {
    name: DataTypes.STRING,
    type: DataTypes.STRING
  }, {});
  Category.associate = function(models) {
    Category.hasMany(models.Transaction, {
      foreignKey: 'categoryId',
      as: 'transactions',
      onDelete: 'CASCADE',
    });
  };
  return Category;
};
