'use strict';
module.exports = (sequelize, DataTypes) => {
  const Transaction = sequelize.define('Transaction', {
    userId: DataTypes.INTEGER,
    bankId: DataTypes.INTEGER,
    categoryId: DataTypes.INTEGER,
    date: DataTypes.DATE,
    amount: DataTypes.STRING,
    score: {
      type: DataTypes.STRING
    },
    name: DataTypes.STRING,
    type: DataTypes.STRING,
    status: DataTypes.STRING,
    observation: DataTypes.TEXT
  }, {});
  Transaction.associate = function(models) {
    Transaction.belongsTo(models.User, {
      foreignKey: 'userId',
      targetKey: 'id'
    })

    Transaction.belongsTo(models.Bank, {
      foreignKey: 'bankId',
      targetKey: 'id'
    })

    Transaction.belongsTo(models.Category, {
      foreignKey: 'categoryId',
      targetKey: 'id'
    })
  };
  return Transaction;
};
