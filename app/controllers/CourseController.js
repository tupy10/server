const {Course} = require("../models");

module.exports = {
    async list(req, res) {
        try {
            const courses = await Course.findAll()
            return res.json(courses)
        } catch (err) {
            console.error("Erro list courses", err)
            return res.json(err)
        }
    },

    async create(req, res){
        const {
            name,
            duration,
            score,
            status,
        } = req.body;

        try {
            let course = await Course.create({
                name,
                duration,
                score,
                status,
            });

            const data = {
                id: course.id,
                name: course.name,
            }

            return res.json(data);
        } catch (error) {
            console.error('Error create', error);
            if(error.errors[0]){
                return res.status(422).json(error.errors[0].message)
            }

            return res.json(error)
        }
    },

    async show(req, res){
        try {
            const course = await Course.findAll({
                where: {
                    id: req.params.id
                }
            });
            return res.json(course);
        } catch (err) {
            console.error("Error in list: ", err);
            return res.status(422).json(err)
        }
    },

    async update(req, res){
        const {
            name,
            duration,
            score,
            status,
        } = req.body;

        const id = req.params.id;

        try {
            const course = await Course.update({
                name,
                duration,
                score,
                status,
            }, {
                where: {id: id},
            });

            console.debug('Course', course)

            return res.json({msg: `Course ${name} updated!`});
        } catch (error) {
            return res.json({msg: `Course ${name} error`}, error);
        }
    },

    async delete(req, res){
        try {
            await Course.destroy({
                where: {
                    id: req.params.id
                }
            });

            return res.json({msg: `Removed ID ${req.params.id} successfully!`});
        } catch (err) {
            return console.err("Error remove: ", err);
        }
    },
}
