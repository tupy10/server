const {User} = require("../models");

module.exports = {
    async list(req, res) {
        try {
            const users = await User.findAll()

            return res.json(users)
        } catch (err) {
            console.error("Erro list users", err)
            return res.json(err)
        }
    },

    async create(req, res){
        const {name, email, password} = req.body;
        // console.debug(name, email, password)
        try {
            let user = await User.create({name, email, password});

            const data = {
                id: user.id,
                name: user.name,
                email: user.email
            }

            return res.json(data);
        } catch (error) {
            console.error('Error create', error);
            if(error.errors[0]){
                return res.status(422).json(error.errors[0].message)
            }

            return res.json(error)
        }
    },

    async show(req, res){
        try {
            const user = await User.findAll({
                where: {
                    id: req.params.id
                },
                attributes: {
                    exclude: ['password']
                }
            });
            return res.json(user);
        } catch (err) {
            console.error("Error in list: ", err);
            return res.status(422).json(er)
        }
    },

    async update(req, res){
        const { name, email } = req.body;
        const id = req.params.id;

        try {
            await User.update({name, email}, {
                where: {
                    id: { id }
                }
            });
            return res.json({msg: `User ${name} updated!`});
        } catch (error) {
            return res.json({msg: `User ${name} error`}, error);
        }
    },

    async delete(req, res){
        try {
            await User.destroy({
                where: {
                    id: req.params.id
                }
            });

            return res.json({msg: `Removed ID ${req.params.id} successfully!`});
        } catch (err) {
            return console.err("Error remove: ", err);
        }
    },
}
