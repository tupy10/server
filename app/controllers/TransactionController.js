const {Transaction} = require("../models");

module.exports = {
    async list(req, res) {
        try {
            const transactions = await Transaction.findAll()
            return res.json(transactions)
        } catch (err) {
            console.error("Erro list transactions", err)
            return res.json(err)
        }
    },

    async create(req, res){
        const {
            userId,
            bankId,
            categoryId,
            date,
            amount,
            score,
            name,
            type,
            status,
            observation,
        } = req.body;

        try {
            let transaction = await Transaction.create({
                userId,
                bankId,
                categoryId,
                date,
                amount,
                score,
                name,
                type,
                status,
                observation,
            });

            const data = {
                id: transaction.id,
                name: transaction.name,
            }

            return res.json(data);
        } catch (error) {
            console.error('Error create', error);
            if(error.errors[0]){
                return res.status(422).json(error.errors[0].message)
            }

            return res.json(error)
        }
    },

    async show(req, res){
        try {
            const transaction = await Transaction.findAll({
                where: {
                    id: req.params.id
                }
            });
            return res.json(transaction);
        } catch (err) {
            console.error("Error in list: ", err);
            return res.status(422).json(err)
        }
    },

    async update(req, res){
        const {
            userId,
            bankId,
            categoryId,
            date,
            amount,
            score,
            name,
            type,
            status,
            observation,
        } = req.body;

        const id = req.params.id;

        try {
            const transaction = await Transaction.update({
                userId,
                bankId,
                categoryId,
                date,
                amount,
                score,
                name,
                type,
                status,
                observation,
            }, {
                where: {id: id},
            });

            console.debug('Transaction', transaction)

            return res.json({msg: `Transaction ${name} updated!`});
        } catch (error) {
            return res.json({msg: `Transaction ${name} error`}, error);
        }
    },

    async delete(req, res){
        try {
            await Transaction.destroy({
                where: {
                    id: req.params.id
                }
            });

            return res.json({msg: `Removed ID ${req.params.id} successfully!`});
        } catch (err) {
            return console.err("Error remove: ", err);
        }
    },
}
