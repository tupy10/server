const {Entity} = require("../models");

module.exports = {
    async list(req, res) {
        try {
            const entities = await Entity.findAll()
            return res.json(entities)
        } catch (err) {
            console.error("Erro list entities", err)
            return res.json(err)
        }
    },

    async create(req, res){
        const {
            name,
            userId,
        } = req.body;

        try {
            let entity = await Entity.create({
                name,
                userId,
            });

            const data = {
                id: entity.id,
                name: entity.name,
            }

            return res.json(data);
        } catch (error) {
            console.error('Error create', error);
            if(error.errors[0]){
                return res.status(422).json(error.errors[0].message)
            }

            return res.json(error)
        }
    },

    async show(req, res){
        try {
            const entity = await Entity.findAll({
                where: {
                    id: req.params.id
                }
            });
            return res.json(entity);
        } catch (err) {
            console.error("Error in list: ", err);
            return res.status(422).json(err)
        }
    },

    async update(req, res){
        const {
            name,
            userId,
        } = req.body;

        const id = req.params.id;

        try {
            const entity = await Entity.update({
                name,
                userId,
            }, {
                where: {id: id},
            });

            console.debug('Entity', entity)

            return res.json({msg: `Entity ${name} updated!`});
        } catch (error) {
            return res.json({msg: `Entity ${name} error`}, error);
        }
    },

    async delete(req, res){
        try {
            await Entity.destroy({
                where: {
                    id: req.params.id
                }
            });

            return res.json({msg: `Removed ID ${req.params.id} successfully!`});
        } catch (err) {
            return console.err("Error remove: ", err);
        }
    },
}
