const {User} = require('../models');
const crypto = require('crypto');
const mailer = require('../../config/mail');
const {from} = require('../../config').mail;

module.exports = {
    async register(req, res) {
        const {name, email, password, confirmPassword} = req.body;

        if (password !== confirmPassword) {
            return res.status(422).json({message: "password not mach"})
        }

        try {
            let user = await User.create({name, email, password});

            const data = await user.authorize()

            return res.json(data)
        } catch (err) {
            if (err.errors && err.errors[0]) {
                return res.status(422).json(err.errors[0].message);
            }

            return res.status(422).json({errors: "Error register user", trace: err})
        }
    },

    async login(req, res) {
        const {email, password} = req.body;

        try {
            let auth = await User.authenticate(email, password);
            return res.json(auth);
        } catch (error) {
            console.error("error login", error)
            if (error.message){
                return res.status(400).json(error.message);
            }

            return res.status(422).json("Error authenticating user")
        }
    },

    async forgotPassword(req, res) {
        const { email } = req.body;

        try {
            const user = await User.findOne({ where: { email } });

            if (!user) {
                return res.status(422).json({error: "User not found"})
            }

            user.rememberToken = crypto.randomBytes(20).toString('hex');
            user.rememberTokenExpires = new Date().setHours(new Date().getHours() + 1);

            await user.save();

            await mailer.sendMail({
                to: email,
                from,
                subject: 'Reset password',
                template: 'auth/forgot-password',
                context: {token: user.rememberToken},
            }, (error) => {
                if (error) {
                    return res.status(422).json({error: 'Cannot send forgot password email'})
                }

                return res.status(200)
            })

            return res.json({message: "Email sent"});

        } catch (err) {
            console.error("error send email",err)
            return res.status(400).json({error: "Error on forgot password"})
        }
    },

    async resetPassword(req, res) {
        const {email, token, password} = req.body;

        try {
            const user = await User.findOne({
                where: { email },
                attributes: {
                    include: ['rememberToken', 'rememberTokenExpires']
                }
            });

            if (!user) {
                return res.status(422).json({error: "User not found"})
            }

            console.table({
                token,
                userToken: user.rememberToken
            })

            if (token !== user.rememberToken) {
                return res.status(422).json({error: "Token invalid"})
            }

            if(new Date() > user.rememberTokenExpires) {
                return res.status(422).json({error: "Token expired, generate a new"})
            }

            user.password = password;
            user.rememberToken = null;
            user.rememberTokenExpires = null;

            await user.save();

            return res.json({message: "Successfully"})

        } catch (error) {
            return res.status(422).json({error: 'Cannot reset password, try again'});
        }
    }
}
