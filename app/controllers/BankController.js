const {Bank} = require("../models");

module.exports = {
    async list(req, res) {
        try {
            const banks = await Bank.findAll()
            return res.json(banks)
        } catch (err) {
            console.error("Erro list banks", err)
            return res.json(err)
        }
    },

    async create(req, res){
        const {code, name, trading, personType, numberSocial} = req.body;
        try {
            let bank = await Bank.create({code, name, trading, personType, numberSocial});

            const data = {
                id: bank.id,
                code: bank.code,
                name: bank.name,
            }

            return res.json(data);
        } catch (error) {
            console.error('Error create', error);
            if(error.errors[0]){
                return res.status(422).json(error.errors[0].message)
            }

            return res.json(error)
        }
    },

    async show(req, res){
        try {
            const bank = await Bank.findAll({
                where: {
                    id: req.params.id
                }
            });
            return res.json(bank);
        } catch (err) {
            console.error("Error in list: ", err);
            return res.status(422).json(err)
        }
    },

    async update(req, res){
        const { code, name, trading, personType, numberSocial } = req.body;
        const id = req.params.id;

        try {
            const bank = await Bank.update({code, name, trading, personType, numberSocial}, {
                where: {id: id},
            });

            console.debug('Bank', bank)

            return res.json({msg: `Bank ${name} updated!`});
        } catch (error) {
            return res.json({msg: `Bank ${name} error`}, error);
        }
    },

    async delete(req, res){
        try {
            await Bank.destroy({
                where: {
                    id: req.params.id
                }
            });

            return res.json({msg: `Removed ID ${req.params.id} successfully!`});
        } catch (err) {
            return console.err("Error remove: ", err);
        }
    },
}
