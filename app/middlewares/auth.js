const jwt = require("jsonwebtoken")
const { jwtSecret } = require("../../config").app

module.exports = (req, res, next) => {
    const authHeader = req.headers.authorization;

    if (!authHeader) {
        return res.status(401).json({error: "No token provided"})
    }

    const parts = authHeader.split(" ");

    if (! parts.length === 2) {
        return res.status(401).json({error: "Token error"})
    }

    const [ schema, token ] = parts;

    if (!/^Bearer$/i.test(schema)) {
        return res.status(401).json({error: "Token error formatting"})
    }

    jwt.verify(token, jwtSecret, (err, decoded) => {
        if (err) {
            return res.status(401).json({error: "Token invalid"})
        }

        req.authId = decoded.id;
        return next();
    })
}
