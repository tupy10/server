const Config = require('./config/index.js')
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const cors = require('cors');
const helmet = require('helmet')

const routes = require('./routes')

const {HTTP_PORT} = Config.app

app.use(helmet());

//https://expressjs.com/en/advanced/best-practice-security.html
app.disable('x-powered-by')

app.use( cors() );
app.use(require('cookie-parser')());
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}));
app.use(routes)
app.listen(HTTP_PORT, () => {
    console.debug("listen on port: " + HTTP_PORT)
})
